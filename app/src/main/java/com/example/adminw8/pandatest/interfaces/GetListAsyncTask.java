package com.example.adminw8.pandatest.interfaces;

import com.example.adminw8.pandatest.communications.responces.WorkObject;

import java.util.List;

/**
 * Created by adminw8 on 13/06/2016.
 */
public interface GetListAsyncTask {
    void processGetListSucceed(List<WorkObject> result);
    void processGetListFailure(String output);

}
