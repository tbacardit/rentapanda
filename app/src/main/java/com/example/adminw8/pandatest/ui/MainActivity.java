package com.example.adminw8.pandatest.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;

import com.example.adminw8.pandatest.R;
import com.example.adminw8.pandatest.communications.responces.WorkObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity  {
    public static List<WorkObject> answer = new ArrayList<>();
    public static String mode ="ALL";
    public static String city ="ALL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Fragment f = new WorkListFragment();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            f.setExitTransition(new Slide(Gravity.LEFT));
        }
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.flContent, f, WorkListFragment.class.getName())
                .commit();

    }
    public void updateTitleInActionBar(String title){
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(title);
        }
    }
    public ArrayList<WorkObject> clearBy (String criteria){
        mode=criteria;
        if(mode.equals("ALL")){
            return (ArrayList<WorkObject>) answer;
        } else {
            ArrayList<WorkObject> returnMe=new ArrayList<>();
            for(WorkObject a : answer){
                if(a.getStatus().equals(mode)){
                    returnMe.add(a);
                    Log.d("TAG3", a.get__status());
                }
            }
            return returnMe;
        }
    }
    public ArrayList<WorkObject> clearByCity (String criteria){
        city=criteria;
        if(city.equals("EVERYWHERE")){
            return (ArrayList<WorkObject>) answer;
        } else {
            ArrayList<WorkObject> returnMe=new ArrayList<>();
            for(WorkObject a : answer){
                if(a.getJob_city().equals(city)){
                    returnMe.add(a);
                }
            }
            return returnMe;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
