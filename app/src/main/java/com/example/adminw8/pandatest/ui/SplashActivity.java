package com.example.adminw8.pandatest.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.example.adminw8.pandatest.R;
import com.example.adminw8.pandatest.communications.responces.WorkObject;
import com.example.adminw8.pandatest.communications.tasks.GetListTask;
import com.example.adminw8.pandatest.interfaces.GetListAsyncTask;

import java.util.List;

/**
 * Created by adminw8 on 14/06/2016.
 */
public class SplashActivity extends Activity implements GetListAsyncTask {

    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.splash_screen);
        GetListTask mListTask = new GetListTask();
        mListTask.mParentTask = this;
        mListTask.execute();

    }

    @Override
    public void processGetListSucceed(List<WorkObject> result) {
        MainActivity.answer=result;
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void processGetListFailure(String output) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        Toast.makeText(this, getString(R.string.error_try_again), Toast.LENGTH_LONG).show();
    }
}
