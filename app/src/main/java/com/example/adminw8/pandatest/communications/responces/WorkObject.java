package com.example.adminw8.pandatest.communications.responces;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by adminw8 on 13/06/2016.
 */
public class WorkObject {
    @SerializedName("__status")
    @Expose
    private String __status;
    @SerializedName("customer_name")
    @Expose
    private String customer_name;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("extras")
    @Expose
    private String extras;
    @SerializedName("job_date")
    @Expose
    private String job_date;
    @SerializedName("order_duration")
    @Expose
    private String order_duration;
    @SerializedName("order_id")
    @Expose
    private String order_id;
    @SerializedName("order_time")
    @Expose
    private String order_time;
    @SerializedName("payment_method")
    @Expose
    private String payment_method;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("recurrency")
    @Expose
    private String recurrency;
    @SerializedName("job_city")
    @Expose
    private String job_city;
    @SerializedName("job_latitude")
    @Expose
    private float job_longitude;
    @SerializedName("job_longitude")
    @Expose
    private float job_latitude;
    @SerializedName("job_postalcode")
    @Expose
    private String job_postalcode;
    @SerializedName("job_street")
    @Expose
    private String job_street;
    @SerializedName("status")
    @Expose
    private String status;

    public String get__status() {
        return __status;
    }

    public void set__status(String __status) {
        this.__status = __status;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getJob_date() {
        return job_date;
    }

    public void setJob_date(String job_date) {
        this.job_date = job_date;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public String getOrder_duration() {
        return order_duration;
    }

    public void setOrder_duration(String order_duration) {
        this.order_duration = order_duration;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_time() {
        return order_time;
    }

    public void setOrder_time(String order_time) {
        this.order_time = order_time;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getRecurrency() {
        return recurrency;
    }

    public void setRecurrency(String recurrency) {
        this.recurrency = recurrency;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getJob_city() {
        return job_city;
    }

    public void setJob_city(String job_city) {
        this.job_city = job_city;
    }

    public float getJob_longitude() {
        return job_longitude;
    }

    public void setJob_longitude(float job_longitude) {
        this.job_longitude = job_longitude;
    }

    public String getJob_postalcode() {
        return job_postalcode;
    }

    public void setJob_postalcode(String job_postalcode) {
        this.job_postalcode = job_postalcode;
    }

    public float getJob_latitude() {
        return job_latitude;
    }

    public void setJob_latitude(float job_latitude) {
        this.job_latitude = job_latitude;
    }

    public String getJob_street() {
        return job_street;
    }

    public void setJob_street(String job_street) {
        this.job_street = job_street;
    }

    public String getStatus() {
        return status;
    }
    public String getDirecion(){
        return job_street+" "+job_postalcode+" "+job_city;
    }
}
