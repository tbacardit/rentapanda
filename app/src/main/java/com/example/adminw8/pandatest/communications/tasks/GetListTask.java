package com.example.adminw8.pandatest.communications.tasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.example.adminw8.pandatest.communications.responces.WorkObject;
import com.example.adminw8.pandatest.interfaces.GetListAsyncTask;
import com.google.gson.Gson;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by adminw8 on 13/06/2016.
 */
public class GetListTask extends AsyncTask<Void, Void, List<WorkObject>> {

    public GetListAsyncTask mParentTask;
    private String error = null;
    private String result = "";
    private ProgressDialog dialog;


    @Override
    protected void onPostExecute(List<WorkObject> result) {
        try {
            if (result != null) {
                mParentTask.processGetListSucceed(result);
            } else {
                mParentTask.processGetListFailure("ERROR");
            }
        } catch(Exception ex) {
            mParentTask.processGetListFailure("ERROR");
        }
    }

    @Override
    protected List<WorkObject> doInBackground(Void... params) {

        List<WorkObject> response = new ArrayList<>();
        try {
            Gson gson = new Gson();
            URL url = new URL("http://private-14c693-rentapanda.apiary-mock.com/jobs");
            String strResponse ="";
            String line;
            BufferedReader br=new BufferedReader(new InputStreamReader(url.openStream()));
            while ((line=br.readLine()) != null) {
                strResponse+=line;
            }
            br.close();
            if (!strResponse.contains("ExceptionMessage")){
                try {
                    JSONArray jsonArray = new JSONArray(strResponse);
                    List<WorkObject> listObject = new ArrayList<WorkObject>();
                    for (int i=0; i<jsonArray.length(); i++) {
                        response.add(gson.fromJson(jsonArray.getString(i),WorkObject.class));
                        String[] parts =  response.get(i).getJob_date().substring(0, 10).split("-");
                        response.get(i).setJob_date(parts[2]+"."+parts[1]+"."+parts[0]);
                        Log.d("TAG is",  response.get(i).getJob_date());
                    }
                } catch (Exception e) {
                    Log.d("TAG", String.valueOf(e));
                }
            }else{
                response=null;
            }
        } catch(Exception ex) {
            Log.d("TAG", ex.getMessage());
        }
        return response;
    }

    @Override
    protected void onCancelled() {
        mParentTask.processGetListFailure("CANCELLED");
    }
}
