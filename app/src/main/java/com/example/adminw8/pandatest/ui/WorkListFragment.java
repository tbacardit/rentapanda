package com.example.adminw8.pandatest.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.adminw8.pandatest.R;
import com.example.adminw8.pandatest.classes.CustomWorkListAdapter;
import com.example.adminw8.pandatest.communications.responces.WorkObject;
import com.example.adminw8.pandatest.communications.tasks.GetListTask;
import com.example.adminw8.pandatest.interfaces.GetListAsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adminw8 on 13/06/2016.
 */
public class WorkListFragment extends Fragment implements GetListAsyncTask {
    //list to search by the direction/name/extra/payment method
    private List<WorkObject> answer;
    public ListView listViewAs;
    private View view;
    private CustomWorkListAdapter adapterList;
    TextView errorRefresh;
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.list_fragment, container, false);
        setHasOptionsMenu(true);
        ((MainActivity) getActivity()).updateTitleInActionBar(getString(R.string.list_title));
        //((MainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //call async
                GetList();
            }
        });
        answer = ((MainActivity) getActivity()).clearBy(MainActivity.mode);
        if(answer.size()==0){
            errorRefresh = (TextView) view.findViewById(R.id.refreshTxt);
            errorRefresh.setVisibility(View.VISIBLE);
        }
        listViewAs = (ListView) view.findViewById(R.id.box_list);
        adapterList = new CustomWorkListAdapter(getActivity(), answer);
        listViewAs.setAdapter(adapterList);
        adapterList.notifyDataSetChanged();
        listViewAs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                     long id) {
                //String uri = String.format(getResources().getConfiguration().locale, "http://maps.google.com/maps?daddr=%f,%f", answer.get(position).getJob_longitude(), answer.get(position).getJob_latitude());
                String uri = String.format(getResources().getConfiguration().locale, "geo:%f,%f?q=%s", adapterList.getPositionID(position).getJob_longitude(), adapterList.getPositionID(position).getJob_latitude(), answer.get(position).getDirecion());
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                getActivity().startActivity(intent);

            }
        });
        // TextFilter
        listViewAs.setTextFilterEnabled(true);
        EditText editTxt = (EditText) view.findViewById(R.id.editTxt);

        editTxt.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count < before) {
                    // We're deleting char so we need to reset the adapter data
                    adapterList.resetData();
                }

                adapterList.getFilter().filter(s.toString());

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))   || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    Log.i("TAG","Enter pressed");
                }
                return false;
            }
        });

        return view;

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.findItem(R.id.current_filter).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                //Log.d("printing", curr);
                String[] lines = {"ALL", "FULFILLED", "PENDING TO START", "INVOICED", "CANCELLED CUSTOMER", "ERROR INTERNAL"};
                LayoutInflater inflater1 = getActivity().getLayoutInflater();
                final View dialogView = inflater1.inflate(R.layout.dialog_change_filter, null);
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                final Spinner spinner = (Spinner) dialogView.findViewById(R.id.lane_spinner_input);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_custom, lines);
                adapter.setDropDownViewResource(R.layout.spinner_item_custom);
                spinner.setAdapter(adapter);
                for (int i = 0; i < lines.length; i++) {
                    if (MainActivity.mode.equals(lines[i])) {
                        spinner.setSelection(i);
                    }
                }
                //Allow in-box change via amount
                builder.setTitle(R.string.title_line_mode)
                        .setView(dialogView)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                answer = ((MainActivity) getActivity()).clearBy(spinner.getSelectedItem().toString());
                                adapterList = new CustomWorkListAdapter(getActivity(), answer);
                                listViewAs.setAdapter(adapterList);
                                adapterList.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //
                            }
                        })
                        .show();
                return true;
            }
        });
        menu.findItem(R.id.current_city).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                //Log.d("printing", curr);
                ArrayList<String> lines = new ArrayList<String>();
                for(WorkObject k : MainActivity.answer){
                    if(!lines.contains(k.getJob_city())){
                        lines.add(k.getJob_city());
                    }
                }
                lines.add("EVERYWHERE");
                LayoutInflater inflater1 = getActivity().getLayoutInflater();
                final View dialogView = inflater1.inflate(R.layout.dialog_change_filter, null);
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                final Spinner spinner = (Spinner) dialogView.findViewById(R.id.lane_spinner_input);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_custom, lines);
                adapter.setDropDownViewResource(R.layout.spinner_item_custom);
                spinner.setAdapter(adapter);
                for (int i = 0; i < lines.size(); i++) {
                    if (MainActivity.city.equals(lines.get(i))) {
                        spinner.setSelection(i);
                    }
                }
                //Allow in-box change via amount
                builder.setTitle(R.string.title_line_location)
                        .setView(dialogView)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                answer = ((MainActivity) getActivity()).clearByCity(spinner.getSelectedItem().toString());
                                adapterList = new CustomWorkListAdapter(getActivity(), answer);
                                listViewAs.setAdapter(adapterList);
                                adapterList.notifyDataSetChanged();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //
                            }
                        })
                        .show();
                return true;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    public void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        view.findViewById(R.id.progressBox).setVisibility(show ? View.VISIBLE : View.GONE);
        view.findViewById(R.id.progressBox).animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.findViewById(R.id.progressBox).setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    private void GetList(){
        showProgress(true);
        GetListTask mListTask = new GetListTask();
        mListTask.mParentTask = this;
        mListTask.execute();
    }

    @Override
    public void processGetListSucceed(List<WorkObject> result) {
        showProgress(false);
        errorRefresh.setVisibility(View.GONE);
        if (answer!=result){
            MainActivity.answer=result;
            answer = ((MainActivity) getActivity()).clearBy(MainActivity.mode);
            adapterList = new CustomWorkListAdapter(getActivity(), answer);
            listViewAs.setAdapter(adapterList);
            adapterList.notifyDataSetChanged();
        }
    }

    @Override
    public void processGetListFailure(String output) {
        showProgress(false);
        Snackbar snackbar = Snackbar.make(view, getString(R.string.error_try_again), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

}
